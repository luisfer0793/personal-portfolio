import React from 'react';

import { Link } from 'react-router-dom';

import styles from './Navigation.module.css';

const Navigation = props => (
    <nav className={styles.Navigation}>
        <div className={styles.NavigationWrapper}>
            <img src="" alt="my portfolio logo"/>
            <ul className={styles.NavigationMenu}>
                <li className={styles.NavigationMenuItem}><Link className={styles.NavigationMenuLink} to="/about">About</Link></li>
                <li className={styles.NavigationMenuItem}><Link className={styles.NavigationMenuLink} to="/skills">Skills</Link></li>
                <li className={styles.NavigationMenuItem}><Link className={styles.NavigationMenuLink} to="/my-work">My Work</Link></li>
                <li className={styles.NavigationMenuItem}><Link className={styles.NavigationMenuLink} to="/contact">Contact</Link></li>
            </ul>
        </div>
    </nav>
);

export default Navigation;