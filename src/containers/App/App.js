import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Navigation from '../../components/Navigation/Navigation';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appName: 'My Portfolio'
        }
    }

    render() {
        return (
            <React.Fragment>
                <Router>
                    <Navigation />
                </Router>
            </React.Fragment>
        );
    }
}

export default App;
